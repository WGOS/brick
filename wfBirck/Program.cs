﻿using System;
using System.Windows.Forms;

namespace wfBirck
{
    static class Program
    {
        public static string Version = "v1.0 alpha";
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
