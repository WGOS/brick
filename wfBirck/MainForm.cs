﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

// TODO добавить комменты
namespace wfBirck
{
    public partial class MainForm : Form
    {
        private KeyboardHook kh = new KeyboardHook();
        private Process txProc;
        private IntPtr txHandle;
        private Thread brickThread, checkThread;
        private static bool isTxRunning, isBirckOnSpace;

        public MainForm()
        {
            InitializeComponent();

            Text = "Кирпич " + Program.Version;

            kh.KeyPressed += hookHK;
            kh.RegisterHotKey(wfBirck.ModifierKeys.Control, Keys.F);

            checkThread = new Thread(checkForTx);
            checkThread.Start();

            brickThread = new Thread(putBrickOnSpacebar);
            brickThread.Start();
            brickThread.Suspend();
        }

        private void putBrickOnSpacebar()
        {
            while (true)
            {
                Thread.Sleep(40);
                if (SetForegroundWindow(txHandle))
                {
                    Invoke(new Action(() => { SendKeys.Send(" "); }));
                }
            }
        }

        private void checkForTx()
        {
            while (true)
            {
                Thread.Sleep(300);
                txProc = Process.GetProcessesByName("tankix").FirstOrDefault();
                if(txProc != null)
                {
                    lbTxState.ForeColor = Color.GreenYellow;
                    lbTxState.Invoke(new Action(() => { lbTxState.Text = "Tanki X запущены"; }));
                    txHandle = txProc.MainWindowHandle;
                    isTxRunning = true;
                }
                else
                {
                    brickThread.Suspend();
                    lbTxState.ForeColor = Color.Orange;
                    lbTxState.Invoke(new Action(() => { lbTxState.Text = "Запустите Tanki X"; }));
                    isTxRunning = false;
                }
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            #region crutch
            /*
              ( `-.__.-' )
               `-.    .-'
                  \  /
                   ||
                   ||
                  //\\
                 //  \\
                ||    ||
                ||____||
                ||====||
                 \\  //
                  \\//
                   ||
                   ||
                   ||
                   ||
                   ||
                   ||
                   ||
                   ||
                   []
            */
            Environment.Exit(0);
            #endregion
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Process.Start("https://gitlab.com/WGOS/brick/");
        }

        private void hookHK(object sender, KeyPressedEventArgs e)
        {
            if (isBirckOnSpace)
            {
                isBirckOnSpace = false;
                brickThread.Suspend();
            }
            else
            {
                isBirckOnSpace = true;
                brickThread.Resume();

            }
                

        }

        [DllImport("user32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

    }
}
